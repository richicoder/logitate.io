# Logitate.io Source
=============================

Source code for https://logitate.io



## Credits
	Template:
		aj@lkn.io | @ajlkn
	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)